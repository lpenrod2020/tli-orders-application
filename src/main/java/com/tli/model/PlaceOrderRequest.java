package com.tli.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class PlaceOrderRequest extends Order {
	
	@NotNull
	private List<Order.LineItem> orderItems;
	
	
}