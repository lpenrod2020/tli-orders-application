package com.tli.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
	
	@NotNull
	private Integer orderId;
	
	@NotNull
	private String status;
	
	@NotNull
	private List<LineItem> lineItems;
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class LineItem {
		
		@NotNull
		private Integer lineNumber;
		
		@NotNull
		private String productName;
		
		@Positive
		@NotNull
		private Float price;
		
		@Size(min = 1)
		@NotNull
		private Integer qty;
		
		public Integer getLineNumber() {
			return lineNumber;
		}
		
		public void setLineNumber(Integer lineNumber) {
			this.lineNumber = lineNumber;
		}
		
		public String getProductName() {
			return productName;
		}
		
		public void setProductName(String productName) {
			this.productName = productName;
		}
		
		public Float getPrice() {
			return price;
		}
		
		public void setPrice(Float price) {
			this.price = price;
		}
		
		public Integer getQty() {
			return qty;
		}
		
		public void setQty(Integer qty) {
			this.qty = qty;
		}
		
	}
	
	@NotNull
	private Timestamp timestamp;
	
	public Integer getOrderId() {
		return orderId;
	}
	
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	

}