package com.tli.model;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class ViewOrCancelOrderRequest {
	
	@NotNull
	private Integer orderNumber;
	
}