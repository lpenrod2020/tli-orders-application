package com.tli.service;

import com.tli.model.Order;
import com.tli.model.PlaceOrderRequest;
import com.tli.model.ViewOrCancelOrderRequest;

public class TliOrderService {
	
	public TliOrderService() {};
	
	public Order viewOrder(ViewOrCancelOrderRequest request) {
		
		//Validate request
		
		//Call Dao layer for data
		
		
		return null;
	}
	
	public Order placeOrder(PlaceOrderRequest request) {
		
		//Validate request
		
		//Call Dao layer for data
		
		return null;
	}
	
	public Order cancelOrder(ViewOrCancelOrderRequest request) {
		
		//Validate request
		
		//Call Dao layer for data
		
		return null;
	}
	
	public Order changeQuantity(Order request) {
		
		//Validate request
		
		//Call Dao layer for data
		
		return null;
	}
	
	public Order removeLineItem(Order request) {
		
		//Validate request
		
		//Call Dao layer for data
		
		return null;
	}
	
}