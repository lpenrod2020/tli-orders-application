package com.tli.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tli.model.Order;
import com.tli.model.PlaceOrderRequest;
import com.tli.model.ViewOrCancelOrderRequest;
import com.tli.service.TliOrderService;

@RestController
public class TliOrderController {
	
	final TliOrderService service;
	
	@Autowired
	public TliOrderController(
			final TliOrderService service) {
		this.service = service;
	}
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<?> viewOrder(@RequestBody final ViewOrCancelOrderRequest viewOrderRequest){
		
		Order viewOrderResponse = this.service.viewOrder(viewOrderRequest);
		
		return ResponseEntity.ok(viewOrderResponse);
		
	}
	
	@PostMapping(value = "/placeOrder")
	public ResponseEntity<?> placeOrder(@RequestBody final PlaceOrderRequest placeOrderRequest){
		
		Order placeOrderResponse = this.service.placeOrder(placeOrderRequest);
		
		return ResponseEntity.ok(placeOrderResponse);
	}
	
	@PutMapping(value = "/cancelOrder")
	public ResponseEntity<?> cancelOrder(@RequestBody final ViewOrCancelOrderRequest cancelOrderRequest){
		
				
		return ResponseEntity.ok(this.service.cancelOrder(cancelOrderRequest));
	}
	
	@PutMapping(value = "/changeQuantity")
	public ResponseEntity<?> changeQuantity(@RequestBody final Order changeQuantityRequest){
		
		Order changeQuantityResponse = this.service.changeQuantity(changeQuantityRequest);
		
		return ResponseEntity.ok(changeQuantityResponse);
		
	}
	
	@PutMapping(value = "/removeLineItem")
	public ResponseEntity<?> removeLineItem(@RequestBody final Order removeLineItemRequest){
		
		Order removeLineItemResponse = this.service.removeLineItem(removeLineItemRequest);
		
		return ResponseEntity.ok(removeLineItemResponse);
	}
	
	
	
}
